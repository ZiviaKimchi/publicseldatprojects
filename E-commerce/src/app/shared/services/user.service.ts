import { User } from "../models/user.model";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { Observable } from "rxjs/Rx";
import { Injectable } from "@angular/core";

@Injectable()
export class UserService {
    constructor(private http: HttpClient) { }
    isUser(user: User): Observable<boolean> {
        return this.http.post("http://localhost:8080/api/users",user)
            .map((response: boolean) => response)
            .catch((response: HttpErrorResponse) => Observable.throw(response));
    }
}