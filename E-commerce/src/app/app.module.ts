import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AppRoutingModule } from "./app-routing.module";
import {HttpClientModule} from "@angular/common/http";
import "rxjs/Rx"
import { LoginComponent } from './login/login.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MdSelectModule,MdButtonModule,MdInputModule,MdSliderModule,MdSidenavModule} from '@angular/material';
import { UserService } from "./shared/services/user.service";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MdSelectModule,
    MdButtonModule,
    MdInputModule,
    MdSliderModule,
    MdSidenavModule
  ],
  providers: [UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
